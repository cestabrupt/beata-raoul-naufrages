# ~/ABRÜPT/BEATA RAOUL/NAUFRAGES/*

La [page de ce livre](https://abrupt.ch/beata-raoul/naufrages/) sur le réseau.

## Sur le livre


La modernité dresse le mot frontière comme une cloison entre le vide et le vide, n’y résonnent les cultures, que reste-t-il de leur partage. L’humain s’affaire, pense sa liberté contre celle des autres, l’humain assassine l’autre, sa liberté, il en fait son affaire, il hurle dans l’ivresse oublieuse de son hybridité animale.

*Naufrages* est l’interrogation des traversées irréelles qui déforment les masques, placent l’altérité au pourtour de ce qui croit encore en son centre. L’autre est le masque de soi, les liens se disloquent et la géographie demeure indifférente à la folie ou à la noyade. *Naufrages* est une voix des morts qui cherche son écho dans un monde des flux, dans un émiettement des cultures, dans chaque être qui se cramponne à l’idée de ne pas être l’autre, de ne pas être le masque de soi.

*Ce livre n’est pas un livre. Ce livre est un programme.*

## Sur l'autrice

Née en 1996 à Stockholm, Beata Raoul grandit à Montréal avant de retourner en Europe poursuivre des études de philosophie et de sciences informatiques à Berlin. Elle entame à présent une thèse d’anthropologie numérique. Face à ses recherches scientifiques, sa poésie s’entend comme une parallaxe qui interroge la précarité de notre raison animale.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est dédié au domaine public. Il est mis à disposition selon les termes de la Licence Creative Commons Zero (CC0 1.0 Universel).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
